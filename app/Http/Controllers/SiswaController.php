<?php

namespace App\Http\Controllers;

use App\Models\siswa;
use Illuminate\Http\Request;

class siswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $datas = siswa::all();
            
            return view('siswa.index',compact('datas'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $model = new siswa;
        return view('siswa.create',compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new siswa;
        $model->nama =$request->nama;
        $model->alamat =$request->alamat;
        $model->tanggal_lahir =$request->tanggal_lahir;
        $model->no_telepon =$request->no_telepon;
        $model->save();
        return redirect('siswa');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = siswa::find($id);
        return view('siswa.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = siswa::find($id);
        $model->nama =$request->nama;
        $model->alamat =$request->alamat;
        $model->tanggal_lahir =$request->tanggal_lahir;
        $model->no_telepon =$request->no_telepon;
        $model->update();

        return redirect('siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = siswa::find($id);
        $model->delete();
        return redirect('siswa');
    }
}
