<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>edit</title>
</head>
<body>
    <form action="{{url('siswa')}}" method="POST">
        @csrf 
        <input type="hidden" name="_method" value="PATCH">
        <div class="row g-3 align-items-center">
  <div class="col-auto">
    <label class="col-form-label">NAMA</label>
  </div>
  <div class="col-auto">
    <input type="text"  name="nama" value="{{$model->nama}}">
  </div>
  <div class="col-auto">
    <label class="col-form-label">ALAMAT</label>
  </div>
  <div class="col-auto">
    <input type="text"  name="alamat" value="{{$model->alamat}}">
  </div>
  <div class="col-auto">
    <label class="col-form-label">TANGGAL LAHIR</label>
  </div>
  <div class="col-auto">
    <input type="text"  name="tanggal_lahir" value="{{$model->tanggal_lahir}}">
  </div>
  <div class="col-auto">
    <label class="col-form-label">NO.Telp</label>
  </div>
  <div class="col-auto">
    <input type="text"  name="no_telepon" value="{{$model->no_telepon}}">
  </div>
  <div class="modal-footer">
                   
                    <button type="sumbit" class="btn btn-primary">simpan</button>
                </div>

    </form>

</body>
</html>