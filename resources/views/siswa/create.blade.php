<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah</title>
</head>
<body>
    <form action="{{url('siswa')}}" method="POST">
        @csrf 
        <div class="row g-3 align-items-center">
  <div class="col-auto">
    <label class="col-form-label">NAMA</label>
  </div>
  <div class="col-auto">
    <input type="text"  name="nama">
  </div>
  <div class="col-auto">
    <label class="col-form-label">ALAMAT</label>
  </div>
  <div class="col-auto">
    <input type="text"  name="alamat">
  </div>
  <div class="col-auto">
    <label class="col-form-label">TANGGAL LAHIR</label>
  </div>
  <div class="col-auto">
    <input type="text"  name="tanggal_lahir">
  </div>
  <div class="col-auto">
    <label class="col-form-label">NO.Telp</label>
  </div>
  <div class="col-auto">
    <input type="text"  name="no_telepon">
  </div>
      <button type="submit">Simpan</button>

    </form>

</body>
</html>