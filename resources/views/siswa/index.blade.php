<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>data siswa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <h3 style="margin-left: 500px;">Daftar Siswa Aktif</h3>
<br/>
    <a class="btn btn-warning" href="{{url('siswa/create')}}">Tambah</a>
    <br/>
    
   <table class="table">
       <tr>
           <th>NAMA</th>
           <th>ALAMAT</th>
           <th>TANGGAL LAHIR</th>
           <th>NO.Telp</th>
           <th colspan="2">AKSI</th>
       </tr>
       @foreach($datas as $key=>$value)
       <tr>
           <td>{{$value->nama}}</td>
           <td>{{$value->alamat}}</td>
           <td>{{$value->tanggal_lahir}}</td>
           <td>{{$value->no_telepon}}</td>
           <td><a class="btn btn-primary" href="{{url('siswa/'.$value->id.'/edit')}}">edit</a></td>
                        <td>
                            <form action="{{url('siswa/'.$value->id)}}" method="POST">
                           @csrf 
                           <input type="hidden" name="_method" value="DELETE">
                           <button class="btn btn-secondary" type="submit" >hapus</button>
                        </form>
                        </td>
       </tr>
       @endforeach
   </table> 
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</html>